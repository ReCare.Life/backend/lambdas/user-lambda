import json
import os

import boto3
from botocore.exceptions import ClientError

from encoder_class import DecimalEncoder


def lambda_handler(event, context):

    if event["path"] == "/users" and event["httpMethod"] == "GET":
        try:

            users = _format_users(list_users())
            return _response(200, {"success": True, "users": users})

        except ClientError as e:
            print(e.response["Error"]["Message"])
            return _response(500, {"status": e.response["Error"]["Message"]})


def list_users():
    client = __get_cognito_client()
    user_pool_id = os.getenv("USER_POOL_ID")
    response = client.list_users(
        UserPoolId=user_pool_id,
        AttributesToGet=["sub", "email", "given_name", "family_name"],
    )
    return response


def __get_cognito_client():
    client = boto3.client("cognito-idp")
    return client


def _format_users(users):
    formatted_users = []
    for user in users["Users"]:
        formatted_user = {
            "sub": user["Attributes"][0]["Value"],
            "given_name": user["Attributes"][1]["Value"],
            "family_name": user["Attributes"][2]["Value"],
            "email": user["Attributes"][3]["Value"],
            "created_at": user["UserCreateDate"].isoformat()[:-9] + "Z",
        }
        formatted_users.append(formatted_user)
    return formatted_users


def _response(status_code, json_body):
    body = json.dumps(json_body, cls=DecimalEncoder)

    return {
        "statusCode": status_code,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
        },
        "body": body,
    }
